      subroutine MV_replace(n,cha,ifmv,title)
C
      character cha_v,cha_u,cha0,cha_ia*6,cha_ia0*6
      character*200 cha(n),cha1(6),cha2(6),line,line1,line0
      character tt(6),cha_all*6,cha_al*6,cha_be*6,title*(*),cha_all0*10
      integer mz_all(6),ico(7),ibra(7),iket(7),icom(7)
      real(kind=8) c0,c1,c2,c3
      character*200 cha_ntype
C
      include 'MV.i'
C
      line=cha(1)
      call read_coif(line,c0)
      call NJ_trim(line,k1,k2)
      cha_all=' ';iall=0;cha_u=' ';mz_all=0
      kk=k1;iu0=0;iu1=0;l=0
111   i1=index(line(kk:k2),cha_sum)
      if(i1.ne.0)then
       i1=i1+kk-1
       i2=index(line(i1+1:k2),'}')+i1-1
       if(l.eq.1)then
         iu1=i1-1
         l=0
       endif
       if(line(i1+i_sum:i1+i_sum).eq.'U0')then
         cha_u='U0'
         iu0=i1
         iu1=k2
         l=1
       else
         i1=i1+i_sum
         do j=i1,i2
          iall=iall+1
          cha_all(iall:iall)=line(j:j)
          mz_all(iall)=1
         enddo
         i3=index(line(i2+1:k2),'^{')+i2
         i4=index(line(i3+1:k2),'}')+i3
         if(index(line(i3:i4),'\beta').ne.0)then
          jj=iall+1
          do j=i2,i1,-1
           jj=jj-1
           mz_all(jj)=-1
          enddo
         endif
       endif
       kk=i2+1
       goto 111
      endif
C
      im=0;ir=0
      do i=1,iall
       cha0=cha_all(i:i)
       if(index(cha_rst,cha0).ne.0)cycle
       im=im+1
      enddo
      if(iall-im.gt.0)ir=1
C      
      iblock1=index(line,'<')
      cha_v=' '
      if(iblock1.ne.0)cha_v=line(iblock1+1:iblock1+1)
      iblock2=index(line,'>')
C
      n_type=-1
      cha_ntype=' ';tt=' '
      line=cha(2)
      cha_mv(4)=line
C
      call NJ_trim(line,k1,k2)
      cha0=line(k1:k1)
      k3=index(line,'}(')
      if(k3.eq.0)return
      cha_ntype=line(k1:k3)
      k3=index(line,'(')
      k4=index(line,')')
      if(k3.eq.0)return
      ni=0
      do i1=k3+1,k4-1
       cha0=line(i1:i1)
       ni=ni+1;tt(ni)=cha0
      enddo
C
200   nte=0;mte=1
       line=cha(3)
      kkk=0
      if(kkk.eq.0.and.im+ir.le.0.and.n_type.ne.-1)return
      endif
       call NJ_trim(line,k1,k2)
       kk=k1;nco=0;ico=0;ibra=0;iket=0;kte=0
300    i1=index(line(kk:k2),'_{')
       if(i1.ne.0)then
        nco=nco+1
        ico(nco)=i1+kk-2
        ibra(nco)=index(line(ico(nco)+1:k2),'(')+ico(nco)
        iket(nco)=index(line(ico(nco)+1:k2),')')+ico(nco)
        kk=iket(nco)+1
        goto 300
       endif
C
      ia0=7;jco=0;ja0=0;cha_ia0=' ';ll0=0;ii0=7
      do 400 i=1,nco
       ll=0;ia=0;ja=0;cha_ia=' ';ii=0
       if(ir.ne.0.and.cha_u.ne.'U0')ll=1
       jm=0
       do j=ibra(i)+1,iket(i)-1
        cha0=line(j:j)
        if(cha0.eq.'U0')then
         ll=1
         cycle
        endif
        if(index('efgGhHmnoOpP',cha0).ne.0)jm=1
        if(index(cha_all,cha0).ne.0)cycle
        if(index(cha_ia,cha0).ne.0)cycle
        ia=ia+1
        if(ia.gt.6)goto 400
        cha_ia(ia:ia)=cha0
        if(index('abcdefgh',cha0).ne.0)ja=ja+1
       enddo
        if(jm.eq.0)goto 400
        if(iket(i)-ibra(i)-1-ia.eq.0)goto 400
       do k=1,ni
         if(ll.eq.1.and.index(cha_rst,tt(k)).ne.0)cycle
         if(index(line(ibra(i)+1:iket(i)-1),tt(k)).ne.0)cycle
         if(index(cha_ia,tt(k)).ne.0)cycle
         ia=ia+1
         if(ia.gt.6)goto 400
         cha_ia(ia:ia)=tt(k)
        if(index('abcdefgh',tt(k)).ne.0)ja=ja+1
       enddo
       ii=ia
       if(ll.eq.1.and.cha_v.eq.'V')ii=ii+1
       if(ii.gt.ii0)cycle
       if(ii.eq.ii0.and.ll.eq.1)cycle
       if(ii.eq.ii0.and.index(cha_ia,'V').ne.0.and.ja.ge.ja0)cycle
       if(ii.eq.ii0.and.ja.ge.ja0)cycle
       ia0=ia;ja0=ja;jco=i;cha_ia0=cha_ia;ll0=ll;ii0=ii
400   continue
C
      cha_ia=cha_ia0;ia=ia0
      ii=ia
      if(ll0.eq.1.and.cha_v.eq.'V')ii=ii+1
      call NJ_trim(title,k1,k2)
      if(ii.gt.6)goto 2000
      cha_mv(5)=line(ico(jco):iket(jco))
      ial=0;ibe=0;cha_al=' ';cha_be=' '
      do i=ibra(jco)+1,iket(jco)-1
        cha0=line(i:i)
        if(cha0.eq.'U0')cycle
        if(index(cha_ia,cha0).ne.0)cycle
        i1=index(cha_all,cha0)
        if(mz_all(i1).eq.1)then
         ial=ial+1;cha_al(ial:ial)=cha0
        else
         ibe=ibe+1;cha_be(ibe:ibe)=cha0
        endif
      enddo
      if(ll0.eq.0)goto 1000
      do i=1,iall
       cha0=cha_all(i:i)
       if(index(cha_rst,cha0).eq.0)cycle
       if(mz_all(i).eq.1)then
         ial=ial+1;cha_al(ial:ial)=cha0
        else
         ibe=ibe+1;cha_be(ibe:ibe)=cha0
        endif
      enddo
      goto 1000
C
2000  if(ir.eq.0.and.kkk.eq.0)return
      if(kkk.eq.0.and.cha_u.eq.'U0'.and.n_type.ne.-1)return
      if(cha_u.eq.'U0'.and.n_type.eq.-1.and.tt(1).ne.'U0')return
C
      ia=0;cha_ia=' '
      i=0;ial=0;ibe=0;cha_al=' ';cha_be=' '
      do i=1,ni
        if(index(cha_rst,tt(i)).ne.0)then
          i1=index(cha_all(1:iall),tt(i))
          if(mz_all(i1).eq.1)then
           ial=ial+1;cha_al(ial:ial)=tt(i)
          else
           ibe=ibe+1;cha_be(ibe:ibe)=tt(i)
          endif
          cycle
        endif
        if(index(cha_ia,tt(i)).ne.0)cycle
        ia=ia+1
        if(ia.gt.6.and.kkk.eq.0)return
        cha_ia(ia:ia)=tt(i)
      enddo
      ii=ia
      if(cha_v.eq.'V')ii=ii+1
      if(ii.gt.6.and.kkk.eq.0)return
C
      ll0=1
1000  line=char(ii+ichar('O'))//'_{'
      nf(ii)=nf(ii)+1
      call NJ_trim(line,k1,k2)
      i0=nf(ii);i2=1000;i3=0
      do i=1,4
       i1=i0/i2
       i0=i0-i1*i2
       i2=i2/10
       if(i1.ne.0)i3=1
       if(i3.eq.0)cycle
       line(k2+1:k2+1)=char(i1+ichar('0'))
       k2=k2+1
       if(i0.ne.0)cycle
       do j=i+1,4
        line(k2+1:k2+1)='0';k2=k2+1
       enddo
       exit
      enddo
      line(k2+1:k2+1)='}';k2=k2+1
      if(ii.gt.0)then
       line(k2+1:k2+1)='(';k2=k2+1
       if(ll0.eq.1.and.cha_v.eq.'V')then
        line(k2+1:k2+1)='V';k2=k2+1
       endif
       if(ia.eq.0)line(k2+1:k2+1)=')'
       if(ia.gt.0)then
         call NJ_trim(title,k1,k2)
         ia0=0
         do i=k1,k2
           cha0=title(i:i)
           if(cha0.eq.' ')cycle
           ia0=ia0+1;cha_ia0(ia0:ia0)=cha0
         enddo
         i0=0
         do i=1,ia
          cha0=cha_ia(i:i)
          if(index(cha_ia0,cha0).ne.0)i0=i0+1
         enddo
         cha_all0=cha_ia0(1:ia0)//cha_all(1:iall)
         do i=1,ia-1
          do j=i+1,ia
           i2=index(cha_all0,cha_ia(i:i))
           j2=index(cha_all0,cha_ia(j:j))
           if(i2.lt.j2)cycle
           cha0=cha_ia(i:i);cha_ia(i:i)=cha_ia(j:j);cha_ia(j:j)=cha0
          enddo
         enddo
        call NJ_trim(line,k1,k2)
        line=line(k1:k2)//cha_ia(1:ia)//')'
       endif
      endif
      call NJ_trim(line,k1,k2)
      cha_mv(2)=line(k1:k2)//'&=&'
C
      line=' '
      if(ll0.eq.1.and.cha_u.eq.'U0')line=cha(1)(iu0:iu1)
      call NJ_trim(line,k1,k2)
      if(ial.gt.0)then
         do i=1,ial-1
          do j=i+1,ial
           i2=index(cha_all0,cha_al(i:i))
           j2=index(cha_all0,cha_al(j:j))
           if(i2.lt.j2)cycle
           cha0=cha_al(i:i);cha_al(i:i)=cha_al(j:j);cha_al(j:j)=cha0
          enddo
         enddo
       line=line(k1:k2)//'\sum_{'//cha_al(1:ial)//'}'
      endif
      call NJ_trim(line,k1,k2)
      if(ibe.gt.0)then
         do i=1,ibe-1
          do j=i+1,ibe
           i2=index(cha_all0,cha_be(i:i))
           j2=index(cha_all0,cha_be(j:j))
           if(i2.lt.j2)cycle
           cha0=cha_be(i:i);cha_be(i:i)=cha_be(j:j);cha_be(j:j)=cha0
          enddo
         enddo
       line=line(k1:k2)//'\sum_{'//cha_be(1:ibe)//'}^{\beta}'
      endif
      call NJ_trim(line,k1,k2)
      if(ll0.eq.1)line=line(k1:k2)//cha(1)(iblock1:iblock2)
      cha_mv(3)=line
C
      ifmv=1
      iii=imv
      if(index(cha_mv(2),'V').ne.0.and.title(1:1).eq.'V')iii=imv1
      do i=1,nmv
        line=cha_mv(i)
        call NJ_trim(line,k1,k2)
        if(line(k1:k1).eq.' ')cycle
        write(iii,'(a)')line(k1:k2)
        write(inme,'(a)')line(k1:k2)
      enddo
      write(iii,'()')
C
      end
C      
