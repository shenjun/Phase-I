      program DERIVE
      parameter(m=3,ntot=1000,nham=4,inp=2,itmp=3,itmp1=4,itex=5)
      integer frac,numhp(ntot),num(m)
      integer indh(ntot),ish(ntot,ntot)
      integer indp(ntot),isp(ntot,ntot)
      integer ind_sum(6)
      integer,allocatable:: ind(:)
      character cha*200,cha1*200,cha2*200
      character line1*200,line2*200
      character,allocatable:: cha_typ(:)*200
      open(inp,file='input')
      read(inp,*)np,nh
      cha=' ';cha1=' '
      n=0;num=0
C
      indp=0;isp=0
      indh=0;ish=0
      nt=0
      do ih=0,nh;do ip=0,np
       if(ih.eq.0.and.ip.eq.0)cycle
       if(ih-ip.ne.nh-np)cycle
       nt=nt+1;is=0
       indh(nt)=ih;indp(nt)=ip
       do jh=0,ih;do jp=0,ip
        if(nh.le.np)then
         if(jh.gt.jp)cycle
         if(ih-jh.gt.ip-jp)cycle
        else
         if(jh.lt.jp)cycle
         if(ih-jh.lt.ip-jp)cycle
        endif
        is=is+1
        numhp(nt)=is
        ish(is,nt)=jh;isp(is,nt)=jp
       enddo;enddo
      enddo;enddo
C
       do ileft=1,nt;do isleft=1,numhp(ileft)
        num(1)=indh(ileft)+indp(ileft)
        if(np.eq.nh)then
         cha='R'//char(ileft+ichar('0'))//char(isleft-1+ichar('A'))
        else
         cha='R'
     &//char(indp(ileft)+ichar('0'))//char(isp(isleft,ileft)+ichar('A'))
     &//char(indh(ileft)+ichar('0'))//char(ish(isleft,ileft)+ichar('A'))
        endif
        call NJ_trim(cha,k1,k2)
        open(itex,file=cha(k1:k2)//'.tex') 
        write(itex,'(a)')
     & '\documentclass[a4paper,fleqn,1pt,ccstart]{article}'
        write(itex,'(a)')
     & '\usepackage{txfonts}'
        iline1=0
        do i=1,indh(ileft)
         if(i.le.indh(ileft)-ish(isleft,ileft))then
          line1(iline1+1:iline1+1)=char(ichar('i')+i-1)
          iline1=iline1+1
         else
          line1(iline1+1:iline1+9)='\tilde{'//char(ichar('i')+i-1)//'}'
          iline1=iline1+9
         endif
        enddo
        iline2=0
        do i=1,indp(ileft)
         if(i.le.indp(ileft)-isp(isleft,ileft))then
          line2(iline2+1:iline2+1)=char(ichar('a')+i-1)
          iline2=iline2+1
         else
          line2(iline2+1:iline2+9)='\tilde{'//char(ichar('a')+i-1)//'}'
          iline2=iline2+9
         endif
        enddo
        if(indh(ileft)*indp(ileft).ne.0)then
         line1='\Phi_{'//line1(1:iline1)//'}^{'//line2(1:iline2)//'}'
        elseif(indh(ileft).ne.0)then
         line1='\Phi_{'//line1(1:iline1)//'}'
        elseif(indp(ileft).ne.0)then
         line1='\Phi^{'//line2(1:iline2)//'}'
        endif
        call NJ_trim(line1,iline2,iline1)
        write(itex,'(a)')
     & '\title{Matrix Element $<'//line1(1:iline1)
     &  //'|(\bar{H}\hat{R})_{C}|\Phi_{0}>$}'
        write(itex,'(a)')
     & '\usepackage{fullpage}'
        write(itex,'(a)')
     & '\begin{document}'
        write(itex,'(a)')
     & '\maketitle'
        write(itex,'(a)')
C
        do iright=1,nt;do isright=1,numhp(iright)
         nright=0
         num(3)=indh(iright)+indp(iright)
         do imid=1,nham;do ismid=0,imid
          num(2)=imid*2
          n=num(1)+num(2)+num(3)
          allocate(ind(n))
          i0=0
          i2=2
          do i=1,indh(ileft)
           i0=i0+1
           i1=i
           i3=1
           if(indh(ileft)-i.lt.ish(isleft,ileft))i3=2
           ind(i0)=i3*100+i2*10+i1
          enddo
          i2=1
          do i=1,indp(ileft)
           i0=i0+1
           i1=indp(ileft)-i+1
           i3=1
           if(i.le.isp(isleft,ileft))i3=2
           ind(i0)=i3*100+i2*10+i1
          enddo
          i2=6
          do i=1,imid
           i0=i0+1
           i1=i
           i3=1
           if(imid-i.lt.ismid)i3=2
           ind(i0)=i3*100+i2*10+i1
          enddo
          i2=5
          do i=1,imid
           i0=i0+1
           i1=imid-i+1
           i3=1
           if(i.le.ismid)i3=2
           ind(i0)=i3*100+i2*10+i1
          enddo
          i2=3
          do i=1,indp(iright)
           i0=i0+1
           i1=i
           i3=1
           if(indp(iright)-i.lt.isp(isright,iright))i3=2
           ind(i0)=i3*100+i2*10+i1
          enddo
          i2=4
          do i=1,indh(iright)
           i0=i0+1
           i1=indh(iright)-i+1
           i3=1
           if(i.le.ish(isright,iright))i3=2
           ind(i0)=i3*100+i2*10+i1
          enddo
          call det_frac(num(m),ind(n-num(m)+1),frac)
          call det_frac1(num(m-1),ind(n-num(m)-num(m-1)+1),frac)
          open(itmp,file='tmp')
          call fcon(itmp,m,n,ind,num)
          ind=0;cha=' '
          rewind(itmp)
          open(itmp1,file='tmp1')
          ii=0
          do 
           read(itmp,*,err=100,end=100)ind
           if(ind(1).eq.0)cycle
           call det_sig(n,ind,i0)
           call exchange(m,n,num,ind)
           call reorder(m,n,num,ind,i0)
           write(itmp1,*)i0,ind
           ii=ii+1
          enddo
100       continue
          close(itmp,status='delete')
C
         call combine(itmp1,n,ii)
         ind=0;cha=' '
         rewind(itmp1)
         do 
          read(itmp1,*,err=200,end=200)i0,ind
          nright=nright+1
!         write(itex,*)i0,frac
!         call num2cha(n,cha,ind)
          if(nright.eq.1)then
           write(itex,'(a)')
     & '\begin{eqnarray}'
          if(np.eq.nh)then
           cha2=char(ichar('0')+iright)//char(ichar('A')+isright-1)
          else
           cha2=
     &     char(ichar('0')+indp(iright))
     &     //char(ichar('A')+isp(isright,iright))
     &     //char(ichar('0')+indh(iright))
     &    //char(ichar('A')+ish(isright,iright))
          endif
          call NJ_trim(cha2,k1,k2)
           write(itex,'(a)')'<'//line1(1:iline1)
     & //'|\bar{H}|\hat{R}_{'
     & //cha2(k1:k2)//'}\Phi_{0}> &=&'
          endif
          isum=0
          do i=n-num(3)+1,n
           if(mod(ind(i)/10,10).ne.3)cycle
           isum=isum+1
           ind_sum(isum)=ind(i)
          enddo
          do i=n,n-num(3)+1,-1
           if(mod(ind(i)/10,10).ne.4)cycle
           isum=isum+1
           ind_sum(isum)=ind(i)
          enddo
          call num2cha3(isum,cha2,ind_sum)
          call NJ_trim(cha2,k3,k4)
          ii=frac/i0
          if(ii.eq.1)then
           cha1='+'
          elseif(ii.eq.-1)then
           cha1='-'
          elseif(ii.gt.1)then
           cha1='+\frac{1}{'//char(ichar('0')+ii)//'}'
          elseif(ii.lt.-1)then
           cha1='-\frac{1}{'//char(ichar('0')-ii)//'}'
          endif
          call NJ_trim(cha1,k1,k2)
          write(itex,'(a)')
     &   cha1(k1:k2)//'\sum_{'//cha2(k3:k4)//'}'
          call num2cha3(num(2)/2,cha1,ind(num(1)+1))
          call NJ_trim(cha1,k1,k2)
          call num2cha4(num(2)/2,cha2,ind(num(1)+num(2)/2+1))
          call NJ_trim(cha2,k3,k4)
          write(itex,'(a)')
     & '\bar{h}_{'//char(ichar('0')+imid)//char(ichar('A')+ismid)
     & //'}('//cha1(k1:k2)//cha2(k3:k4)//')'
!         write(itex,'(a)')
!    & '\bar{H}_{'//cha1(k1:k2)//'}^{'//cha2(k3:k4)//'}'
          cha1=' ';cha2=' '
          if(indp(iright).ne.0)
     &    call num2cha3(indp(iright),cha1,ind(num(1)+num(2)+1))
          if(indh(iright).ne.0)
     &    call num2cha4(indh(iright),cha2,
     &    ind(num(1)+num(2)+indp(iright)+1))
          call NJ_trim(cha1,k1,k2)
          call NJ_trim(cha2,k3,k4)
          cha1=cha1(k1:k2)//cha2(k3:k4)
          call NJ_trim(cha1,k1,k2)
          if(np.eq.nh)then
           write(itex,'(a)')
     & 'r_{'//char(ichar('0')+iright)//char(ichar('A')+isright-1)
     & //'}('//cha1(k1:k2)//')'
          else
           write(itex,'(a)')'r_{'
     & //char(ichar('0')+indp(iright))
     & //char(ichar('A')+isp(isright,iright))
     & //char(ichar('0')+indh(iright))
     & //char(ichar('A')+ish(isright,iright))
     & //'}('//cha1(k1:k2)//')'
          endif
          write(itex,'(a)')
     & '\nonumber\\&&'
         enddo
200      continue
         close(itmp1,status='delete')
         deallocate(ind)
        enddo;enddo
        if(nright.eq.0)cycle
        write(itex,'(a)')
     & '\end{eqnarray}'
        write(itex,'()')
       enddo;enddo
       write(itex,'(a)')
     & '\end{document}'
       close(itex)
      enddo;enddo
      end


