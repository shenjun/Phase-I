      subroutine cha_reorder(cha0,cha1,cha2,cha_all,mz_all,cha_de)
      character*200 cha1,cha2,line,line1,line2
      character cha0,cha_ia*6,cha_ia1*6,ch0,ch1*4,ch2*2,cha_all*10
      character cha_de*2,linex*100,liney*100,ch3*2,linez*100
      integer mz_all(10)
      include 'EQN2FOR.i'
C
      j1=index(cha1,'(')+1
      j2=index(cha1,')')-1
      ia=j2-j1+1
      cha_ia=cha1(j1:j2)
C
      nefmn=1
      ifS=0
      iP=0
      j1=index(cha1,cha_f)
      if(j1.ne.0)goto 1000
      j1=index(cha1,cha_i)
      if(j1.ne.0)goto 1000
      j1=index(cha1,cha_h)
      if(j1.eq.0)goto 5000
1000  continue
        ifS=0
        nefmn=1
      j1=index(cha1,'_{')+2
      j2=index(cha1,'}(')-1
      k1=index(cha1,'{')+1
      line=cha1(k1:k1)//cha1(j1:j2)
      call NJ_trim(line,k1,k2)
      do i=1,ia
        ch0=cha_ia(i:i)
        if(ch0.ge.'a'.and.ch0.lt.'i')line(k2+1:k2+1)='P'
        if(ch0.ge.'i'.and.ch0.le.'p')line(k2+1:k2+1)='H'
        if(ch0.ge.'A'.and.ch0.lt.'I')line(k2+1:k2+1)='P'
        if(ch0.ge.'I'.and.ch0.le.'P')line(k2+1:k2+1)='H'
        k2=k2+1
        if(ch0.ge.'a'.and.ch0.lt.'i')iP=iP+1
        if(ch0.ge.'A'.and.ch0.lt.'I')iP=iP+1
        if(index('mnef',ch0).eq.0)nefmn=0
      enddo
      line(k2+1:k2+1)='('
      if(iP.ne.4)goto 4000
      line(k2-3:k2)='APPP'
      if(line(k2-4:k2-4).ne.'B')goto 4000
      if(cha_ia(1:1).ge.'A'.and.cha_ia(1:1).lt.'I')goto 4000
      iP=-4
      line(k2-3:k2)='PAPP'
      goto 4000
C       
5000  continue
!     if(index(cha1,'t_').ne.0)goto 3000
      j1=index(cha1,'_{')+2
      j2=index(cha1,'}')-1
      j3=index(cha1,'(')+1
      j4=index(cha1,')')-1
      if(index(cha1,cha_r).ne.0)then         
       if(index(cha1,'4').eq.0)then
        line=cha1(1:1)//cha1(j1:j1+3)//'('
       elseif(index(cha1,'4A')+index(cha1,'4E').ne.0)then
        line=cha1(1:1)//cha1(j1:j1+3)//'('
       elseif(index(cha1,'4B').ne.0)then
        if(index(cha1,'1001').ne.0)then
          line=cha1(1:1)//cha1(j1:j1+3)//'1('
         else
          line=cha1(1:1)//cha1(j1:j1+3)//'('
        endif
       elseif(index(cha1,'4C').ne.0)then
        if(index(cha1,'1100').ne.0)then
          line=cha1(1:1)//cha1(j1:j1+3)//'1('
        elseif(index(cha1,'0011').ne.0)then
          line=cha1(1:1)//cha1(j1:j1+3)//'2('
         else
          line=cha1(1:1)//cha1(j1:j1+3)//'('
        endif
       elseif(index(cha1,'4D').ne.0)then
        if(index(cha1,'1100').ne.0)then
          line=cha1(1:1)//cha1(j1:j1+3)//'1('
         else
          line=cha1(1:1)//cha1(j1:j1+3)//'('
        endif
       endif
      else
       line=cha1(1:1)//cha1(j1:j2)//'('
      endif
      if(index(cha1,'t_')+index(cha1,'r_').eq.0)then
        ifS=1
      else
       cha_ia1=cha_ia
       i0=0
       do i=1,ia
        if(cha_ia1(i:i).lt.'i'.and.cha_ia1(i:i).ge.'a')cycle
        if(cha_ia1(i:i).lt.'I'.and.cha_ia1(i:i).ge.'A')cycle
        i0=i0+1
        cha_ia(i0:i0)=cha_ia1(i:i)
       enddo
       do i=1,ia
        if(cha_ia1(i:i).ge.'i'.and.cha_ia1(i:i).le.'l')cycle
        if(cha_ia1(i:i).ge.'I'.and.cha_ia1(i:i).le.'L')cycle
        i0=i0+1
        cha_ia(i0:i0)=cha_ia1(i:i)
       enddo
      endif
C
4000  continue
      k1=index(line,'(')
      do i=ia,1,-1
       ch0=cha_ia(i:i)
       i0=index(cha_all,ch0)
       line=line(1:k1)//ch0//',';k1=k1+2
      enddo
      line(k1:k1)=')'
      cha_ia1=cha_ia
      ii=0
      do i=1,ia-1
       do j=i+1,ia
        i0=index(cha_all,cha_ia1(i:i))
        j0=index(cha_all,cha_ia1(j:j))
        if(i0.lt.j0)cycle
        ii=1
        ch0=cha_ia1(i:i);cha_ia1(i:i)=cha_ia1(j:j);cha_ia1(j:j)=ch0
       enddo
      enddo
      cha2=line
      if(ii.eq.0.and.ifS.eq.1)return
C
3000  continue
      line1=char(ichar('A')+ia-1)//cha0//'('
      cha_de=line1(1:2)
      line2=line1
      k2=3;k3=k2
      do i=ia,1,-1
       ch0=cha_ia1(i:i)
       i0=index(cha_all,ch0)
       line1=line1(1:k2)//ch0//','
       k2=k2+2
       call cha_type(mz_all(i0),ch0,ch1,ch2)
       line2=line2(1:k3)//ch1//':'//ch2//','
       k3=k3+8 
      enddo
      line1(k2:k2)=')'
      line2(k3:k3)=')'
      write(io,'(7x,a)')'allocate('//line2(1:k3)//')'
      do i=1,ia
       ch0=cha_ia1(i:i) 
       i0=index(cha_all,ch0)
       call cha_type1(mz_all(i0),ch0,ch2)
!      write(io,'(7x,a)')'do '//ch0//'='//ch1//','//ch2
      enddo
!     write(io,'(9x,a)')line1(1:k2)//'='//line(1:k1)
      do i=1,ia
!      write(io,'(7x,a)')'enddo'
      enddo
C
      i1=index(line,'(')+1
      i2=index(line,')')-1
      j1=index(line1,'(')+1
      j2=index(line1,')')-1
      ii=0
      do i=j1,j2,2
       ch0=line1(i:i)
       i3=index(line(i1:i2),ch0)/2+1
       ii=ii+1
       linex(ii:ii)=char(ichar('0')+i3)
       i0=index(cha_all,ch0)
       call cha_type3(mz_all(i0),ch0,ch2,ch3)
       liney(6*ii-5:6*ii)=ch2//','//ch3//','
      enddo
      if(iP.eq.4)then
       k2=0
       do i=ia,1,-1
        ch0=cha_ia(i:i)
        i0=index(cha_all,ch0)
        if(i.eq.1)call cha_type3(mz_all(i0),ch0,ch2,ch3)
        if(i.ne.1)call cha_type0(mz_all(i0),ch0,ch2,ch3)
        linez=linez(1:k2)//ch2//','//ch3//','
        k2=k2+6
       enddo
      elseif(iP.eq.-4)then
        linez='N2,N3,N1,N3,N2,M2,N1,N3,'
      elseif(nefmn.eq.0)then
       k2=0
       do i=ia,1,-1
        ch0=cha_ia(i:i)
        i0=index(cha_all,ch0)
        call cha_type0(mz_all(i0),ch0,ch2,ch3)
        linez=linez(1:k2)//ch2//','//ch3//','
        k2=k2+6
       enddo
      elseif(index(line,'r4').ne.0)then        !DEA
        call cha_type2(line,linez)
        call NJ_trim(linez,k1,k2)
        do i=2,1,-1
         ch0=cha_ia(i:i)
         i0=index(cha_all,ch0)
         call cha_type0(mz_all(i0),ch0,ch2,ch3)
         linez(K2+1:k2+6)=ch2//','//ch3//','
         k2=k2+6
        enddo
      elseif(index(line,'r')+index(line,'t').ne.0)then        
       k2=0
       do i=ia,1,-1
        ch0=cha_ia(i:i) 
        i0=index(cha_all,ch0)
        call cha_type0(mz_all(i0),ch0,ch2,ch3)
        linez=linez(1:k2)//ch2//','//ch3//','
        k2=k2+6
       enddo
      else
       k2=0
       do i=ia,1,-1
        ch0=cha_ia(i:i) 
        i0=index(cha_all,ch0)
        call cha_type3(mz_all(i0),ch0,ch2,ch3)
        linez=linez(1:k2)//ch2//','//ch3//','
        k2=k2+6
       enddo
      endif
      call NJ_trim(linez,k1,k2)
      write(io,'(7x,a)')'call reorder'//linex(1:ii)//'('//linez(1:k2)
      write(io,'(5x,a)')
     ''& '//liney(1:6*ii)//line(1:i1-2)//','//line1(1:j1-2)//')'
C
      cha2=line1
C
      end
