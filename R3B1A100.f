       subroutine R3B1A100(N0,N1,N2,N3,M2,HR3B1A,
     & K1,K2,K3,K4,K5,K6,K7,K8,K9,K0,
     & FAHH,FAHP,FAPP,FBHH,FBHP,FBPP,
     & VAHHHH,VAHHHP,VAHHPP,VAHPHP,VAHPPP,
     & VBHHHH,VBHHHP,VBHHPH,VBHHPP,VBHPHP,VBHPPH,
     & VBPHPH,VBHPPP,VBPHPP,
     & VCHHHH,VCHHHP,VCHHPP,VCHPHP,VCHPPP,
     & VAAPPP,VBAPPP,VBPAPP,VCAPPP,
     & HAHH,HAHP,HAPP,HBHH,FBHP,HBPP,
     & HAHHHH,HAHHHP,HAHPHH,HAHPHP,HAHPPP,HAPPHP,
     & HBHHHH,HBHHHP,HBHHPH,HBHPHH,HBPHHH,HBHHPP,HBHPHP,HBHPPH,
     & HBPHHP,HBPHPH,HBHPPP,HBPHPP,HBPPHP,HBPPPH,
     & HCHHHH,HCHHHP,HCHPHH,HCHPHP,HCHPPP,HCPPHP,
     & HAAPPP,HAPPAP,HBAPPP,HBPAPP,HBPPAP,HBPPPA,HCAPPP,HCPPAP,
     & t1A,t1B,t2A,t2B,t2C,
     & r2B0A,r3B1A,r3C1B,r4B2A,r4B2A1,r4C2B,r4C2B1,r4C2B2,r4D2C,r4D2C1)
C
       integer a,b,c,e,f,g,h,i,j,k,m,n,o,p
       real*8 FAHH(N0+1:N1,N0+1:N1)
       real*8 FAHP(N1+1:N3,N0+1:N1)
       real*8 FAPP(N1+1:N3,N1+1:N3)
       real*8 FBHH(N0+1:N2,N0+1:N2)
       real*8 FBHP(N2+1:N3,N0+1:N2)
       real*8 FBPP(N2+1:N3,N2+1:N3)
       real*8 VAHHHH(N0+1:N1,N0+1:N1,N0+1:N1,N0+1:N1)
       real*8 VAHHHP(N1+1:N3,N0+1:N1,N0+1:N1,N0+1:N1)
       real*8 VAHHPP(N1+1:N3,N1+1:N3,N0+1:N1,N0+1:N1)
       real*8 VAHPHP(N1+1:N3,N0+1:N1,N1+1:N3,N0+1:N1)
       real*8 VAHPPP(N1+1:N3,N1+1:N3,N1+1:N3,N0+1:N1)
       real*8 VBHHHH(N0+1:N2,N0+1:N1,N0+1:N2,N0+1:N1)
       real*8 VBHHHP(N2+1:N3,N0+1:N1,N0+1:N2,N0+1:N1)
       real*8 VBHHPH(N0+1:N2,N1+1:N3,N0+1:N2,N0+1:N1)
       real*8 VBHHPP(N2+1:N3,N1+1:N3,N0+1:N2,N0+1:N1)
       real*8 VBHPHP(N2+1:N3,N0+1:N1,N2+1:N3,N0+1:N1)
       real*8 VBHPPH(N0+1:N2,N1+1:N3,N2+1:N3,N0+1:N1)
       real*8 VBPHPH(N0+1:N2,N1+1:N3,N0+1:N2,N1+1:N3)
       real*8 VBHPPP(N2+1:N3,N1+1:N3,N2+1:N3,N0+1:N1)
       real*8 VBPHPP(N2+1:N3,N1+1:N3,N0+1:N2,N1+1:N3)
       real*8 VCHHHH(N0+1:N2,N0+1:N2,N0+1:N2,N0+1:N2)
       real*8 VCHHHP(N2+1:N3,N0+1:N2,N0+1:N2,N0+1:N2)
       real*8 VCHHPP(N2+1:N3,N2+1:N3,N0+1:N2,N0+1:N2)
       real*8 VCHPHP(N2+1:N3,N0+1:N2,N2+1:N3,N0+1:N2)
       real*8 VCHPPP(N2+1:N3,N2+1:N3,N2+1:N3,N0+1:N2)
       real*8 VAAPPP(N1+1:N3,N1+1:N3,N1+1:N3,N1+1:M2)
       real*8 VBAPPP(N2+1:N3,N1+1:N3,N2+1:N3,N1+1:M2)
       real*8 VBPAPP(N2+1:N3,N1+1:N3,N2+1:M2,N1+1:N3)
       real*8 VCAPPP(N2+1:N3,N2+1:N3,N2+1:N3,N2+1:M2)
       real*8 HAHH(N0+1:N1,N0+1:N1)
       real*8 HAHP(N1+1:N3,N0+1:N1)
       real*8 HAPP(N1+1:N3,N1+1:N3)
       real*8 HBHH(N0+1:N2,N0+1:N2)
       real*8 HBHP(N2+1:N3,N0+1:N2)
       real*8 HBPP(N2+1:N3,N2+1:N3)
       real*8 HAHHHH(N0+1:N1,N0+1:N1,N0+1:N1,N0+1:N1)
       real*8 HAHHHP(N1+1:N3,N0+1:N1,N0+1:N1,N0+1:N1)
       real*8 HAHPHH(N0+1:N1,N0+1:N1,N1+1:N3,N0+1:N1)
       real*8 HAHPHP(N1+1:N3,N0+1:N1,N1+1:N3,N0+1:N1)
       real*8 HAHPPP(N1+1:N3,N1+1:N3,N1+1:N3,N0+1:N1)
       real*8 HAPPHP(N1+1:N3,N0+1:N1,N1+1:N3,N1+1:N3)
       real*8 HBHHHH(N0+1:N2,N0+1:N1,N0+1:N2,N0+1:N1)
       real*8 HBHHHP(N2+1:N3,N0+1:N1,N0+1:N2,N0+1:N1)
       real*8 HBHPHH(N0+1:N2,N0+1:N1,N2+1:N3,N0+1:N1)
       real*8 HBHHPH(N0+1:N2,N1+1:N3,N0+1:N2,N0+1:N1)
       real*8 HBPHHH(N0+1:N2,N0+1:N1,N0+1:N2,N1+1:N3)
       real*8 HBHPHP(N2+1:N3,N0+1:N1,N2+1:N3,N0+1:N1)
       real*8 HBHPPH(N0+1:N2,N1+1:N3,N2+1:N3,N0+1:N1)
       real*8 HBPHHP(N2+1:N3,N0+1:N1,N0+1:N2,N1+1:N3)
       real*8 HBPHPH(N0+1:N2,N1+1:N3,N0+1:N2,N1+1:N3)
       real*8 HBHPPP(N2+1:N3,N1+1:N3,N2+1:N3,N0+1:N1)
       real*8 HBPPHP(N2+1:N3,N0+1:N1,N2+1:N3,N1+1:N3)
       real*8 HBPHPP(N2+1:N3,N1+1:N3,N0+1:N2,N1+1:N3)
       real*8 HBPPPH(N0+1:N2,N1+1:N3,N2+1:N3,N1+1:N3)
       real*8 HCHHHH(N0+1:N2,N0+1:N2,N0+1:N2,N0+1:N2)
       real*8 HCHHHP(N2+1:N3,N0+1:N2,N0+1:N2,N0+1:N2)
       real*8 HCHPHH(N0+1:N2,N0+1:N2,N2+1:N3,N0+1:N2)
       real*8 HCHPHP(N2+1:N3,N0+1:N2,N2+1:N3,N0+1:N2)
       real*8 HCHPPP(N2+1:N3,N2+1:N3,N2+1:N3,N0+1:N2)
       real*8 HCPPHP(N2+1:N3,N0+1:N2,N2+1:N3,N2+1:N3)
       real*8 HAAPPP(N1+1:N3,N1+1:N3,N1+1:N3,N1+1:M2)
       real*8 HAPPAP(N1+1:N3,N1+1:M2,N1+1:N3,N1+1:N3)
       real*8 HBAPPP(N2+1:N3,N1+1:N3,N2+1:N3,N1+1:M2)
       real*8 HBPAPP(N2+1:N3,N1+1:N3,N2+1:M2,N1+1:N3)
       real*8 HBPPAP(N2+1:N3,N1+1:M2,N2+1:N3,N1+1:N3)
       real*8 HBPPPA(N2+1:M2,N1+1:N3,N2+1:N3,N1+1:N3)
       real*8 HCAPPP(N2+1:N3,N2+1:N3,N2+1:N3,N2+1:M2)
       real*8 HCPPAP(N2+1:N3,N2+1:M2,N2+1:N3,N2+1:N3)
       real*8 t1A(N1+1:N3,N0+1:N1)
       real*8 t1B(N2+1:N3,N0+1:N2)
       real*8 t2A(N1+1:N3,N1+1:N3,N0+1:N1,N0+1:N1)
       real*8 t2B(N2+1:N3,N1+1:N3,N0+1:N2,N0+1:N1)
       real*8 t2C(N2+1:N3,N2+1:N3,N0+1:N2,N0+1:N2)
       real*8 r2B0A(N2+1:N3,N1+1:N3)
       real*8 r3B1A(N2+1:N3,N1+1:N3,N1+1:N3,N0+1:N1)
       real*8 r3C1B(N2+1:N3,N2+1:N3,N1+1:N3,N0+1:N2)
       real*8 r4B2A(N2+1:N3,N1+1:N3,N1+1:M2,N1+1:M2,N0+1:N1,N0+1:N1)
       real*8 r4B2A1(N2+1:M2,M2+1:N3,M2+1:N3,N1+1:M2,N0+1:N1,N0+1:N1)
       real*8 r4C2B(N2+1:N3,N2+1:M2,N1+1:N3,N1+1:M2,N0+1:N2,N0+1:N1)
       real*8 r4C2B1(M2+1:N3,M2+1:N3,N1+1:M2,N1+1:M2,N0+1:N2,N0+1:N1)
       real*8 r4C2B2(N2+1:M2,N2+1:M2,M2+1:N3,M2+1:N3,N0+1:N2,N0+1:N1)
       real*8 r4D2C(N2+1:N3,N2+1:M2,N2+1:M2,N1+1:N3,N0+1:N2,N0+1:N2)
       real*8 r4D2C1(M2+1:N3,M2+1:N3,N2+1:M2,N1+1:M2,N0+1:N2,N0+1:N2)
       real*8 HR3B1A(N2+1:N3,N1+1:N3,N1+1:N3,N0+1:N1)
C
       real*8,allocatable::B1(:,:)
       real*8,allocatable::B2(:,:)
       real*8,allocatable::C1(:,:,:)
       real*8,allocatable::C2(:,:,:)
       real*8,allocatable::D1(:,:,:,:)
       real*8,allocatable::D2(:,:,:,:)
       real*8,allocatable::F2(:,:,:,:,:,:)
C
       real*8,allocatable::U1(:,:,:,:)
       real*8,allocatable::U2(:,:,:,:)
       real*8,allocatable::U3(:,:,:,:)
       real*8,allocatable::Q1(:,:)
       real*8,allocatable::U4(:,:,:,:)
       real*8,allocatable::Q2(:,:)
       real*8,allocatable::U5(:,:,:,:)
       real*8,allocatable::Q3(:,:)
       real*8,allocatable::U6(:,:,:,:)
       real*8,allocatable::Q4(:,:)
       real*8,allocatable::Q6(:,:)
       real*8,allocatable::Q7(:,:)
       real*8,allocatable::Q5(:,:)
       real*8,allocatable::U7(:,:,:,:)
       real*8,allocatable::U8(:,:,:,:)
       real*8,allocatable::U9(:,:,:,:)
       real*8,allocatable::U10(:,:,:,:)
       real*8,allocatable::U11(:,:,:,:)
       real*8,allocatable::U12(:,:,:,:)
       real*8,allocatable::U13(:,:,:,:)
       real*8,allocatable::U14(:,:,:,:)
       real*8,allocatable::U15(:,:,:,:)
       real*8,allocatable::U16(:,:,:,:)
       real*8,allocatable::U17(:,:,:,:)
       real*8,allocatable::U18(:,:,:,:)
       real*8,allocatable::U19(:,:,:,:)
       real*8,allocatable::Q8(:,:)
       real*8,allocatable::U20(:,:,:,:)
       real*8,allocatable::Q9(:,:)
       real*8,allocatable::U21(:,:,:,:)
       real*8,allocatable::Q10(:,:)
       real*8,allocatable::U22(:,:,:,:)
       real*8,allocatable::U23(:,:,:,:)
       real*8,allocatable::U24(:,:,:,:)
       real*8,allocatable::Q11(:,:)
       real*8,allocatable::U25(:,:,:,:)
       real*8,allocatable::Q12(:,:)
       real*8,allocatable::U26(:,:,:,:)
       real*8,allocatable::Q13(:,:)
       real*8,allocatable::U27(:,:,:,:)
       real*8,allocatable::U28(:,:,:,:)
       real*8,allocatable::U29(:,:,:,:)
       real*8,allocatable::U30(:,:,:,:)
       real*8,allocatable::U31(:,:,:,:)
       real*8,allocatable::U32(:,:,:,:)
       real*8,allocatable::U33(:,:,:,:)
       real*8,allocatable::U34(:,:,:,:)
       real*8,allocatable::U35(:,:,:,:)
       real*8,allocatable::U36(:,:,:,:)
       real*8,allocatable::U37(:,:,:,:)
       real*8,allocatable::U38(:,:,:,:)
       real*8,allocatable::U39(:,:,:,:)
       real*8,allocatable::U40(:,:,:,:)
       real*8,allocatable::U41(:,:,:,:)
       real*8,allocatable::U42(:,:,:,:)
       real*8,allocatable::U43(:,:,:,:)
C
       real*8,allocatable::V3B1A(:,:,:,:)
       allocate(V3B1A(M2+1:N3,M2+1:N3,N1+1:M2,N0+1:N1))
       V3B1A=0.0d0
C
       allocate(D1(N2+1:N3,N1+1:N3,N0+1:N1,M2+1:N3))
       call reorder1243(N2,N3,N1,N3,N2,N3,N0,N1,
     & N2,N3,N1,N3,N0,N1,M2,N3,VBHPPP,D1)
       allocate(B2(N2+1:N3,N1+1:N3))
       call reorder12(N2,N3,N1,N3,
     & N2,N3,N1,N3,r2B0A,B2)
       allocate(Q1(N0+1:N1,M2+1:N3))
       I1=K6*K1
       I3=K3*K4
       call DGEMM1(I1,I3,D1,B2,Q1)
       deallocate(D1)
       deallocate(B2)
C
       allocate(D2(N0+1:N1,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder3124(N1,N3,N1,N3,N0,N1,N0,N1,
     & N0,N1,M2,N3,N1,M2,N0,N1,t2A,D2)
       allocate(U4(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K1
       call DGEMM(I1,I2,I3,Q1,D2,U4)
       deallocate(D2)
C
       call
     & sum2341(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U4,-1.000)
       deallocate(U4)
       deallocate(Q1)
C
       allocate(D1(N2+1:N3,N1+1:N3,N0+1:N2,M2+1:N3))
       call reorder1234(N2,N3,N1,N3,N0,N2,N1,N3,
     & N2,N3,N1,N3,N0,N2,M2,N3,VBPHPP,D1)
       allocate(B2(N2+1:N3,N1+1:N3))
       call reorder12(N2,N3,N1,N3,
     & N2,N3,N1,N3,r2B0A,B2)
       allocate(Q2(N0+1:N2,M2+1:N3))
       I1=K6*K2
       I3=K3*K4
       call DGEMM1(I1,I3,D1,B2,Q2)
       deallocate(D1)
       deallocate(B2)
C
       allocate(D2(N0+1:N2,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder3124(N2,N3,N1,N3,N0,N2,N0,N1,
     & N0,N2,M2,N3,N1,M2,N0,N1,t2B,D2)
       allocate(U5(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K2
       call DGEMM(I1,I2,I3,Q2,D2,U5)
       deallocate(D2)
C
       call
     & sum1342(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U5,-1.000)
       deallocate(U5)
       deallocate(Q2)
C
       allocate(D1(N2+1:N3,N1+1:N3,N0+1:N2,N1+1:M2))
       call reorder1234(N2,N3,N1,N3,N0,N2,N1,N3,
     & N2,N3,N1,N3,N0,N2,N1,M2,VBPHPP,D1)
       allocate(B2(N2+1:N3,N1+1:N3))
       call reorder12(N2,N3,N1,N3,
     & N2,N3,N1,N3,r2B0A,B2)
       allocate(Q3(N0+1:N2,N1+1:M2))
       I1=K9*K2
       I3=K3*K4
       call DGEMM1(I1,I3,D1,B2,Q3)
       deallocate(D1)
       deallocate(B2)
C
       allocate(D2(N0+1:N2,M2+1:N3,M2+1:N3,N0+1:N1))
       call reorder3124(N2,N3,N1,N3,N0,N2,N0,N1,
     & N0,N2,M2,N3,M2,N3,N0,N1,t2B,D2)
       allocate(U6(M2+1:N3,M2+1:N3,N0+1:N1,N1+1:M2))
       I1=K9
       I2=K1*K6*K6
       I3=K2
       call DGEMM(I1,I2,I3,Q3,D2,U6)
       deallocate(D2)
C
       call
     & sum1243(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U6, 1.000)
       deallocate(U6)
       deallocate(Q3)
C
       allocate(D1(N2+1:N3,N1+1:N3,N0+1:N2,N0+1:N1))
       call reorder1234(N2,N3,N1,N3,N0,N2,N0,N1,
     & N2,N3,N1,N3,N0,N2,N0,N1,VBHHPP,D1)
       allocate(B2(N2+1:N3,N1+1:N3))
       call reorder12(N2,N3,N1,N3,
     & N2,N3,N1,N3,r2B0A,B2)
       allocate(Q4(N0+1:N2,N0+1:N1))
       I1=K1*K2
       I3=K3*K4
       call DGEMM1(I1,I3,D1,B2,Q4)
       deallocate(D1)
       deallocate(B2)
C
       allocate(B1(N0+1:N1,N0+1:N2))
       call reorder21(N0,N2,N0,N1,
     & N0,N1,N0,N2,Q4,B1)
       allocate(B2(N0+1:N1,N1+1:M2))
       call reorder21(N1,N3,N0,N1,
     & N0,N1,N1,M2,t1A,B2)
       allocate(Q6(N1+1:M2,N0+1:N2))
       I1=K2
       I2=K9
       I3=K1
       call DGEMM(I1,I2,I3,B1,B2,Q6)
       deallocate(B1)
       deallocate(B2)
C
       allocate(B1(N0+1:N2,N1+1:M2))
       call reorder21(N1,M2,N0,N2,
     & N0,N2,N1,M2,Q6,B1)
       allocate(D2(N0+1:N2,M2+1:N3,M2+1:N3,N0+1:N1))
       call reorder3124(N2,N3,N1,N3,N0,N2,N0,N1,
     & N0,N2,M2,N3,M2,N3,N0,N1,t2B,D2)
       allocate(U8(M2+1:N3,M2+1:N3,N0+1:N1,N1+1:M2))
       I1=K9
       I2=K1*K6*K6
       I3=K2
       call DGEMM(I1,I2,I3,B1,D2,U8)
       deallocate(B1)
       deallocate(D2)
C
       call
     & sum1243(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U8,-1.000)
       deallocate(U8)
       deallocate(Q6)
C
       allocate(B2(N0+1:N2,M2+1:N3))
       call reorder21(N2,N3,N0,N2,
     & N0,N2,M2,N3,t1B,B2)
       allocate(Q7(M2+1:N3,N0+1:N1))
       I1=K1
       I2=K6
       I3=K2
       call DGEMM(I1,I2,I3,Q4,B2,Q7)
       deallocate(B2)
C
       allocate(B1(N0+1:N1,M2+1:N3))
       call reorder21(M2,N3,N0,N1,
     & N0,N1,M2,N3,Q7,B1)
       allocate(D2(N0+1:N1,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder3124(N1,N3,N1,N3,N0,N1,N0,N1,
     & N0,N1,M2,N3,N1,M2,N0,N1,t2A,D2)
       allocate(U9(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K1
       call DGEMM(I1,I2,I3,B1,D2,U9)
       deallocate(B1)
       deallocate(D2)
C
       call
     & sum2341(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U9, 1.000)
       deallocate(U9)
       deallocate(Q7)
C
       allocate(B1(N0+1:N1,N0+1:N2))
       call reorder21(N0,N2,N0,N1,
     & N0,N1,N0,N2,Q4,B1)
       allocate(B2(N0+1:N1,M2+1:N3))
       call reorder21(N1,N3,N0,N1,
     & N0,N1,M2,N3,t1A,B2)
       allocate(Q5(M2+1:N3,N0+1:N2))
       I1=K2
       I2=K6
       I3=K1
       call DGEMM(I1,I2,I3,B1,B2,Q5)
       deallocate(B1)
       deallocate(B2)
C
       allocate(B1(N0+1:N2,M2+1:N3))
       call reorder21(M2,N3,N0,N2,
     & N0,N2,M2,N3,Q5,B1)
       allocate(D2(N0+1:N2,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder3124(N2,N3,N1,N3,N0,N2,N0,N1,
     & N0,N2,M2,N3,N1,M2,N0,N1,t2B,D2)
       allocate(U7(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K2
       call DGEMM(I1,I2,I3,B1,D2,U7)
       deallocate(B1)
       deallocate(D2)
C
       call
     & sum1342(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U7, 1.000)
       deallocate(U7)
       deallocate(Q5)
C
       allocate(D1(N0+1:N1,N1+1:N3,N1+1:N3,N0+1:N1))
       call reorder3124(N1,N3,N1,N3,N0,N1,N0,N1,
     & N0,N1,N1,N3,N1,N3,N0,N1,VAHHPP,D1)
       allocate(D2(N0+1:N1,N1+1:N3,N1+1:N3,M2+1:N3))
       call reorder4231(N2,N3,N1,N3,N1,N3,N0,N1,
     & N0,N1,N1,N3,N1,N3,M2,N3,r3B1A,D2)
       allocate(Q8(M2+1:N3,N0+1:N1))
       I1=K1
       I2=K6
       I3=K3*K3*K1
       call DGEMM(I1,I2,I3,D1,D2,Q8)
       deallocate(D1)
       deallocate(D2)
C
       allocate(B1(N0+1:N1,M2+1:N3))
       call reorder21(M2,N3,N0,N1,
     & N0,N1,M2,N3,Q8,B1)
       allocate(D2(N0+1:N1,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder3124(N1,N3,N1,N3,N0,N1,N0,N1,
     & N0,N1,M2,N3,N1,M2,N0,N1,t2A,D2)
       allocate(U20(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K1
       call DGEMM(I1,I2,I3,B1,D2,U20)
       deallocate(B1)
       deallocate(D2)
C
       call
     & sum2341(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U20, 0.500)
       deallocate(U20)
       deallocate(Q8)
C
       allocate(D1(N0+1:N1,N2+1:N3,N1+1:N3,N0+1:N2))
       call reorder4123(N2,N3,N1,N3,N0,N2,N0,N1,
     & N0,N1,N2,N3,N1,N3,N0,N2,VBHHPP,D1)
       allocate(D2(N0+1:N1,N2+1:N3,N1+1:N3,N1+1:M2))
       call reorder4123(N2,N3,N1,N3,N1,N3,N0,N1,
     & N0,N1,N2,N3,N1,N3,N1,M2,r3B1A,D2)
       allocate(Q9(N1+1:M2,N0+1:N2))
       I1=K2
       I2=K9
       I3=K3*K4*K1
       call DGEMM(I1,I2,I3,D1,D2,Q9)
       deallocate(D1)
       deallocate(D2)
C
       allocate(B1(N0+1:N2,N1+1:M2))
       call reorder21(N1,M2,N0,N2,
     & N0,N2,N1,M2,Q9,B1)
       allocate(D2(N0+1:N2,M2+1:N3,M2+1:N3,N0+1:N1))
       call reorder3124(N2,N3,N1,N3,N0,N2,N0,N1,
     & N0,N2,M2,N3,M2,N3,N0,N1,t2B,D2)
       allocate(U21(M2+1:N3,M2+1:N3,N0+1:N1,N1+1:M2))
       I1=K9
       I2=K1*K6*K6
       I3=K2
       call DGEMM(I1,I2,I3,B1,D2,U21)
       deallocate(B1)
       deallocate(D2)
C
       call
     & sum1243(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U21,-1.000)
       deallocate(U21)
       deallocate(Q9)
C
       allocate(D1(N0+1:N1,N2+1:N3,N1+1:N3,N0+1:N2))
       call reorder4123(N2,N3,N1,N3,N0,N2,N0,N1,
     & N0,N1,N2,N3,N1,N3,N0,N2,VBHHPP,D1)
       allocate(D2(N0+1:N1,N2+1:N3,N1+1:N3,M2+1:N3))
       call reorder4123(N2,N3,N1,N3,N1,N3,N0,N1,
     & N0,N1,N2,N3,N1,N3,M2,N3,r3B1A,D2)
       allocate(Q10(M2+1:N3,N0+1:N2))
       I1=K2
       I2=K6
       I3=K3*K4*K1
       call DGEMM(I1,I2,I3,D1,D2,Q10)
       deallocate(D1)
       deallocate(D2)
C
       allocate(B1(N0+1:N2,M2+1:N3))
       call reorder21(M2,N3,N0,N2,
     & N0,N2,M2,N3,Q10,B1)
       allocate(D2(N0+1:N2,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder3124(N2,N3,N1,N3,N0,N2,N0,N1,
     & N0,N2,M2,N3,N1,M2,N0,N1,t2B,D2)
       allocate(U22(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K2
       call DGEMM(I1,I2,I3,B1,D2,U22)
       deallocate(B1)
       deallocate(D2)
C
       call
     & sum1342(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U22, 1.000)
       deallocate(U22)
       deallocate(Q10)
C
       allocate(D1(N0+1:N2,N2+1:N3,N1+1:N3,N0+1:N1))
       call reorder3124(N2,N3,N1,N3,N0,N2,N0,N1,
     & N0,N2,N2,N3,N1,N3,N0,N1,VBHHPP,D1)
       allocate(D2(N0+1:N2,N2+1:N3,N1+1:N3,M2+1:N3))
       call reorder4132(N2,N3,N2,N3,N1,N3,N0,N2,
     & N0,N2,N2,N3,N1,N3,M2,N3,r3C1B,D2)
       allocate(Q11(M2+1:N3,N0+1:N1))
       I1=K1
       I2=K6
       I3=K3*K4*K2
       call DGEMM(I1,I2,I3,D1,D2,Q11)
       deallocate(D1)
       deallocate(D2)
C
       allocate(B1(N0+1:N1,M2+1:N3))
       call reorder21(M2,N3,N0,N1,
     & N0,N1,M2,N3,Q11,B1)
       allocate(D2(N0+1:N1,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder3124(N1,N3,N1,N3,N0,N1,N0,N1,
     & N0,N1,M2,N3,N1,M2,N0,N1,t2A,D2)
       allocate(U25(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K1
       call DGEMM(I1,I2,I3,B1,D2,U25)
       deallocate(B1)
       deallocate(D2)
C
       call
     & sum2341(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U25,-1.000)
       deallocate(U25)
       deallocate(Q11)
C
       allocate(D1(N0+1:N2,N2+1:N3,N2+1:N3,N0+1:N2))
       call reorder4123(N2,N3,N2,N3,N0,N2,N0,N2,
     & N0,N2,N2,N3,N2,N3,N0,N2,VCHHPP,D1)
       allocate(D2(N0+1:N2,N2+1:N3,N2+1:N3,N1+1:M2))
       call reorder4123(N2,N3,N2,N3,N1,N3,N0,N2,
     & N0,N2,N2,N3,N2,N3,N1,M2,r3C1B,D2)
       allocate(Q12(N1+1:M2,N0+1:N2))
       I1=K2
       I2=K9
       I3=K4*K4*K2
       call DGEMM(I1,I2,I3,D1,D2,Q12)
       deallocate(D1)
       deallocate(D2)
C
       allocate(B1(N0+1:N2,N1+1:M2))
       call reorder21(N1,M2,N0,N2,
     & N0,N2,N1,M2,Q12,B1)
       allocate(D2(N0+1:N2,M2+1:N3,M2+1:N3,N0+1:N1))
       call reorder3124(N2,N3,N1,N3,N0,N2,N0,N1,
     & N0,N2,M2,N3,M2,N3,N0,N1,t2B,D2)
       allocate(U26(M2+1:N3,M2+1:N3,N0+1:N1,N1+1:M2))
       I1=K9
       I2=K1*K6*K6
       I3=K2
       call DGEMM(I1,I2,I3,B1,D2,U26)
       deallocate(B1)
       deallocate(D2)
C
       call
     & sum1243(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U26,-0.500)
       deallocate(U26)
       deallocate(Q12)
C
       allocate(D1(N0+1:N2,N2+1:N3,N2+1:N3,N0+1:N2))
       call reorder3124(N2,N3,N2,N3,N0,N2,N0,N2,
     & N0,N2,N2,N3,N2,N3,N0,N2,VCHHPP,D1)
       allocate(D2(N0+1:N2,N2+1:N3,N2+1:N3,M2+1:N3))
       call reorder4123(N2,N3,N2,N3,N1,N3,N0,N2,
     & N0,N2,N2,N3,N2,N3,M2,N3,r3C1B,D2)
       allocate(Q13(M2+1:N3,N0+1:N2))
       I1=K2
       I2=K6
       I3=K4*K4*K2
       call DGEMM(I1,I2,I3,D1,D2,Q13)
       deallocate(D1)
       deallocate(D2)
C
       allocate(B1(N0+1:N2,M2+1:N3))
       call reorder21(M2,N3,N0,N2,
     & N0,N2,M2,N3,Q13,B1)
       allocate(D2(N0+1:N2,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder3124(N2,N3,N1,N3,N0,N2,N0,N1,
     & N0,N2,M2,N3,N1,M2,N0,N1,t2B,D2)
       allocate(U27(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K2
       call DGEMM(I1,I2,I3,B1,D2,U27)
       deallocate(B1)
       deallocate(D2)
C
       call
     & sum1342(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U27,-0.500)
       deallocate(U27)
       deallocate(Q13)
C
       allocate(D1(N1+1:N3,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder1342(N1,N3,N0,N1,N1,N3,N1,N3,
     & N1,N3,M2,N3,N1,M2,N0,N1,HAPPHP,D1)
       allocate(B2(N1+1:N3,M2+1:N3))
       call reorder21(N2,N3,N1,N3,
     & N1,N3,M2,N3,r2B0A,B2)
       allocate(U1(M2+1:N3,M2+1:N3,N1+1:M2,N0+1:N1))
       I1=K1*K9*K6
       I2=K6
       I3=K3
       call DGEMM(I1,I2,I3,D1,B2,U1)
       deallocate(D1)
       deallocate(B2)
C
       V3B1A=V3B1A+U1
       deallocate(U1)
C
       allocate(D1(N2+1:N3,M2+1:N3,M2+1:N3,N0+1:N1))
       call reorder1342(N2,N3,N0,N1,N2,N3,N1,N3,
     & N2,N3,M2,N3,M2,N3,N0,N1,HBPPHP,D1)
       allocate(B2(N2+1:N3,N1+1:M2))
       call reorder12(N2,N3,N1,N3,
     & N2,N3,N1,M2,r2B0A,B2)
       allocate(U2(N1+1:M2,M2+1:N3,M2+1:N3,N0+1:N1))
       I1=K1*K6*K6
       I2=K9
       I3=K4
       call DGEMM(I1,I2,I3,D1,B2,U2)
       deallocate(D1)
       deallocate(B2)
C
       call
     & sum3124(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U2,-1.000)
       deallocate(U2)
C
       allocate(D1(N2+1:N3,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder1342(N2,N3,N0,N1,N2,N3,N1,N3,
     & N2,N3,M2,N3,N1,M2,N0,N1,HBPPHP,D1)
       allocate(B2(N2+1:N3,M2+1:N3))
       call reorder12(N2,N3,N1,N3,
     & N2,N3,M2,N3,r2B0A,B2)
       allocate(U3(M2+1:N3,M2+1:N3,N1+1:M2,N0+1:N1))
       I1=K1*K9*K6
       I2=K6
       I3=K4
       call DGEMM(I1,I2,I3,D1,B2,U3)
       deallocate(D1)
       deallocate(B2)
C
       call
     & sum2134(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U3, 1.000)
       deallocate(U3)
C
       allocate(B1(N0+1:N1,N0+1:N1))
       call reorder21(N0,N1,N0,N1,
     & N0,N1,N0,N1,HAHH,B1)
       allocate(D2(N0+1:N1,M2+1:N3,M2+1:N3,N1+1:M2))
       call reorder4123(N2,N3,N1,N3,N1,N3,N0,N1,
     & N0,N1,M2,N3,M2,N3,N1,M2,r3B1A,D2)
       allocate(U10(M2+1:N3,M2+1:N3,N1+1:M2,N0+1:N1))
       I1=K1
       I2=K9*K6*K6
       I3=K1
       call DGEMM(I1,I2,I3,B1,D2,U10)
       deallocate(B1)
       deallocate(D2)
C
       V3B1A=V3B1A-U10
       deallocate(U10)
C
       allocate(B1(N1+1:N3,M2+1:N3))
       call reorder12(N1,N3,N1,N3,
     & N1,N3,M2,N3,HAPP,B1)
       allocate(D2(N1+1:N3,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder2134(N2,N3,N1,N3,N1,N3,N0,N1,
     & N1,N3,M2,N3,N1,M2,N0,N1,r3B1A,D2)
       allocate(U11(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K3
       call DGEMM(I1,I2,I3,B1,D2,U11)
       deallocate(B1)
       deallocate(D2)
C
       call
     & sum1342(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U11, 1.000)
       deallocate(U11)
C
       allocate(B1(N1+1:N3,N1+1:M2))
       call reorder12(N1,N3,N1,N3,
     & N1,N3,N1,M2,HAPP,B1)
       allocate(D2(N1+1:N3,M2+1:N3,M2+1:N3,N0+1:N1))
       call reorder2134(N2,N3,N1,N3,N1,N3,N0,N1,
     & N1,N3,M2,N3,M2,N3,N0,N1,r3B1A,D2)
       allocate(U12(M2+1:N3,M2+1:N3,N0+1:N1,N1+1:M2))
       I1=K9
       I2=K1*K6*K6
       I3=K3
       call DGEMM(I1,I2,I3,B1,D2,U12)
       deallocate(B1)
       deallocate(D2)
C
       call
     & sum1243(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U12,-1.000)
       deallocate(U12)
C
       allocate(B1(N2+1:N3,M2+1:N3))
       call reorder12(N2,N3,N2,N3,
     & N2,N3,M2,N3,HBPP,B1)
       allocate(D2(N2+1:N3,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder1234(N2,N3,N1,N3,N1,N3,N0,N1,
     & N2,N3,M2,N3,N1,M2,N0,N1,r3B1A,D2)
       allocate(U13(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K4
       call DGEMM(I1,I2,I3,B1,D2,U13)
       deallocate(B1)
       deallocate(D2)
C
       call
     & sum2341(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U13, 1.000)
       deallocate(U13)
C
       allocate(D1(N0+1:N1,N1+1:N3,M2+1:N3,N0+1:N1))
       call reorder4132(N1,N3,N0,N1,N1,N3,N0,N1,
     & N0,N1,N1,N3,M2,N3,N0,N1,HAHPHP,D1)
       allocate(D2(N0+1:N1,N1+1:N3,M2+1:N3,N1+1:M2))
       call reorder4213(N2,N3,N1,N3,N1,N3,N0,N1,
     & N0,N1,N1,N3,M2,N3,N1,M2,r3B1A,D2)
       allocate(U14(M2+1:N3,N1+1:M2,M2+1:N3,N0+1:N1))
       I1=K1*K6
       I2=K9*K6
       I3=K3*K1
       call DGEMM(I1,I2,I3,D1,D2,U14)
       deallocate(D1)
       deallocate(D2)
C
       call
     & sum1324(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U14,-1.000)
       deallocate(U14)
C
       allocate(D1(N0+1:N1,N1+1:N3,N1+1:M2,N0+1:N1))
       call reorder4132(N1,N3,N0,N1,N1,N3,N0,N1,
     & N0,N1,N1,N3,N1,M2,N0,N1,HAHPHP,D1)
       allocate(D2(N0+1:N1,N1+1:N3,M2+1:N3,M2+1:N3))
       call reorder4213(N2,N3,N1,N3,N1,N3,N0,N1,
     & N0,N1,N1,N3,M2,N3,M2,N3,r3B1A,D2)
       allocate(U15(M2+1:N3,M2+1:N3,N1+1:M2,N0+1:N1))
       I1=K1*K9
       I2=K6*K6
       I3=K3*K1
       call DGEMM(I1,I2,I3,D1,D2,U15)
       deallocate(D1)
       deallocate(D2)
C
       V3B1A=V3B1A+U15
       deallocate(U15)
C
       allocate(D1(N1+1:N3,N1+1:N3,M2+1:N3,N1+1:M2))
       call reorder1234(N1,N3,N1,N3,N1,N3,N1,M2,
     & N1,N3,N1,N3,M2,N3,N1,M2,HAAPPP,D1)
       allocate(D2(N1+1:N3,N1+1:N3,M2+1:N3,N0+1:N1))
       call reorder2314(N2,N3,N1,N3,N1,N3,N0,N1,
     & N1,N3,N1,N3,M2,N3,N0,N1,r3B1A,D2)
       allocate(U16(M2+1:N3,N0+1:N1,M2+1:N3,N1+1:M2))
       I1=K9*K6
       I2=K1*K6
       I3=K3*K3
       call DGEMM(I1,I2,I3,D1,D2,U16)
       deallocate(D1)
       deallocate(D2)
C
       call
     & sum1423(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U16, 0.500)
       deallocate(U16)
C
       allocate(D1(N0+1:N1,N2+1:N3,M2+1:N3,N0+1:N1))
       call reorder4132(N2,N3,N0,N1,N2,N3,N0,N1,
     & N0,N1,N2,N3,M2,N3,N0,N1,HBHPHP,D1)
       allocate(D2(N0+1:N1,N2+1:N3,M2+1:N3,N1+1:M2))
       call reorder4123(N2,N3,N1,N3,N1,N3,N0,N1,
     & N0,N1,N2,N3,M2,N3,N1,M2,r3B1A,D2)
       allocate(U17(M2+1:N3,N1+1:M2,M2+1:N3,N0+1:N1))
       I1=K1*K6
       I2=K9*K6
       I3=K4*K1
       call DGEMM(I1,I2,I3,D1,D2,U17)
       deallocate(D1)
       deallocate(D2)
C
       call
     & sum2314(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U17,-1.000)
       deallocate(U17)
C
       allocate(D1(N2+1:N3,N1+1:N3,M2+1:N3,M2+1:N3))
       call reorder1234(N2,N3,N1,N3,N2,M2,N1,N3,
     & N2,N3,N1,N3,M2,N3,M2,N3,HBPAPP,D1)
       allocate(D2(N2+1:N3,N1+1:N3,N1+1:M2,N0+1:N1))
       call reorder1234(N2,N3,N1,N3,N1,N3,N0,N1,
     & N2,N3,N1,N3,N1,M2,N0,N1,r3B1A,D2)
       allocate(U18(N1+1:M2,N0+1:N1,M2+1:N3,M2+1:N3))
       I1=K6*K6
       I2=K1*K9
       I3=K3*K4
       call DGEMM(I1,I2,I3,D1,D2,U18)
       deallocate(D1)
       deallocate(D2)
C
       call
     & sum3412(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U18, 1.000)
       deallocate(U18)
C
       allocate(D1(N2+1:N3,N1+1:N3,M2+1:N3,N1+1:M2))
       call reorder1234(N2,N3,N1,N3,N2,N3,N1,M2,
     & N2,N3,N1,N3,M2,N3,N1,M2,HBAPPP,D1)
       allocate(D2(N2+1:N3,N1+1:N3,M2+1:N3,N0+1:N1))
       call reorder1234(N2,N3,N1,N3,N1,N3,N0,N1,
     & N2,N3,N1,N3,M2,N3,N0,N1,r3B1A,D2)
       allocate(U19(M2+1:N3,N0+1:N1,M2+1:N3,N1+1:M2))
       I1=K9*K6
       I2=K1*K6
       I3=K3*K4
       call DGEMM(I1,I2,I3,D1,D2,U19)
       deallocate(D1)
       deallocate(D2)
C
       call
     & sum2413(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U19,-1.000)
       deallocate(U19)
C
       allocate(D1(N0+1:N2,N2+1:N3,M2+1:N3,N0+1:N1))
       call reorder3142(N2,N3,N0,N1,N0,N2,N1,N3,
     & N0,N2,N2,N3,M2,N3,N0,N1,HBPHHP,D1)
       allocate(D2(N0+1:N2,N2+1:N3,M2+1:N3,N1+1:M2))
       call reorder4123(N2,N3,N2,N3,N1,N3,N0,N2,
     & N0,N2,N2,N3,M2,N3,N1,M2,r3C1B,D2)
       allocate(U23(M2+1:N3,N1+1:M2,M2+1:N3,N0+1:N1))
       I1=K1*K6
       I2=K9*K6
       I3=K4*K2
       call DGEMM(I1,I2,I3,D1,D2,U23)
       deallocate(D1)
       deallocate(D2)
C
       call
     & sum1324(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U23,-1.000)
       deallocate(U23)
C
       allocate(D1(N0+1:N2,N2+1:N3,N1+1:M2,N0+1:N1))
       call reorder3142(N2,N3,N0,N1,N0,N2,N1,N3,
     & N0,N2,N2,N3,N1,M2,N0,N1,HBPHHP,D1)
       allocate(D2(N0+1:N2,N2+1:N3,M2+1:N3,M2+1:N3))
       call reorder4123(N2,N3,N2,N3,N1,N3,N0,N2,
     & N0,N2,N2,N3,M2,N3,M2,N3,r3C1B,D2)
       allocate(U24(M2+1:N3,M2+1:N3,N1+1:M2,N0+1:N1))
       I1=K1*K9
       I2=K6*K6
       I3=K4*K2
       call DGEMM(I1,I2,I3,D1,D2,U24)
       deallocate(D1)
       deallocate(D2)
C
       V3B1A=V3B1A+U24
       deallocate(U24)
C
       allocate(B1(N0+1:N1,N1+1:M2))
       call reorder21(N1,N3,N0,N1,
     & N0,N1,N1,M2,HAHP,B1)
       allocate(F2(N0+1:N1,N1+1:M2,M2+1:N3,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder531246(N2,N3,N1,N3,N1,M2,N1,M2,N0,N1,N0,N1,
     & N0,N1,N1,M2,M2,N3,M2,N3,N1,M2,N0,N1,r4B2A,F2)
       allocate(U28(M2+1:N3,M2+1:N3,N1+1:M2,N0+1:N1))
       I2=K1*K9*K6*K6
       I3=K9*K1
       call DGEMM2(I2,I3,B1,F2,U28)
       deallocate(B1)
       deallocate(F2)
C
       V3B1A=V3B1A+U28
       deallocate(U28)
C
       allocate(D1(N0+1:N1,N0+1:N1,N1+1:M2,N0+1:N1))
       call reorder3412(N1,N3,N0,N1,N0,N1,N0,N1,
     & N0,N1,N0,N1,N1,M2,N0,N1,HAHHHP,D1)
       allocate(F2(N0+1:N1,N0+1:N1,N1+1:M2,M2+1:N3,M2+1:N3,N1+1:M2))
       call reorder563124(N2,N3,N1,N3,N1,M2,N1,M2,N0,N1,N0,N1,
     & N0,N1,N0,N1,N1,M2,M2,N3,M2,N3,N1,M2,r4B2A,F2)
       allocate(U29(M2+1:N3,M2+1:N3,N1+1:M2,N0+1:N1))
       I1=K1
       I2=K9*K6*K6
       I3=K9*K1*K1
       call DGEMM(I1,I2,I3,D1,F2,U29)
       deallocate(D1)
       deallocate(F2)
C
       V3B1A=V3B1A-0.500*U29
       deallocate(U29)
C
       allocate(D1(N0+1:N1,M2+1:N3,N1+1:M2,M2+1:N3))
       call reorder4123(N1,N3,N1,N3,N1,N3,N0,N1,
     & N0,N1,M2,N3,N1,M2,M2,N3,HAHPPP,D1)
       allocate(F2(N0+1:N1,M2+1:N3,N1+1:M2,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder523146(N2,N3,N1,N3,N1,M2,N1,M2,N0,N1,N0,N1,
     & N0,N1,M2,N3,N1,M2,M2,N3,N1,M2,N0,N1,r4B2A,F2)
       allocate(U30(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K9*K6*K1
       call DGEMM(I1,I2,I3,D1,F2,U30)
       deallocate(D1)
       deallocate(F2)
C
       call
     & sum1342(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U30, 1.000)
       deallocate(U30)
C
       allocate(D1(N0+1:N1,N1+1:M2,N1+1:M2,M2+1:N3))
       call reorder4123(N1,N3,N1,N3,N1,N3,N0,N1,
     & N0,N1,N1,M2,N1,M2,M2,N3,HAHPPP,D1)
       allocate(F2(N0+1:N1,N1+1:M2,N1+1:M2,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder523146(N2,N3,N1,N3,N1,M2,N1,M2,N0,N1,N0,N1,
     & N0,N1,N1,M2,N1,M2,M2,N3,N1,M2,N0,N1,r4B2A,F2)
       allocate(U31(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K9*K9*K1
       call DGEMM(I1,I2,I3,D1,F2,U31)
       deallocate(D1)
       deallocate(F2)
C
       call
     & sum1342(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U31, 0.500)
       deallocate(U31)
C
       allocate(D1(N0+1:N1,N1+1:M2,N1+1:M2,N1+1:M2))
       call reorder4123(N1,N3,N1,N3,N1,N3,N0,N1,
     & N0,N1,N1,M2,N1,M2,N1,M2,HAHPPP,D1)
       allocate(F2(N0+1:N1,N1+1:M2,N1+1:M2,M2+1:N3,M2+1:N3,N0+1:N1))
       call reorder534126(N2,N3,N1,N3,N1,M2,N1,M2,N0,N1,N0,N1,
     & N0,N1,N1,M2,N1,M2,M2,N3,M2,N3,N0,N1,r4B2A,F2)
       allocate(U32(M2+1:N3,M2+1:N3,N0+1:N1,N1+1:M2))
       I1=K9
       I2=K1*K6*K6
       I3=K9*K9*K1
       call DGEMM(I1,I2,I3,D1,F2,U32)
       deallocate(D1)
       deallocate(F2)
C
       call
     & sum1243(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U32,-0.500)
       deallocate(U32)
C
       allocate(D1(N0+1:N1,N2+1:M2,M2+1:N3,M2+1:N3))
       call reorder4123(N2,N3,N1,N3,N2,N3,N0,N1,
     & N0,N1,N2,M2,M2,N3,M2,N3,HBHPPP,D1)
       allocate(F2(N0+1:N1,N2+1:M2,M2+1:N3,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder512346(N2,M2,M2,N3,M2,N3,N1,M2,N0,N1,N0,N1,
     & N0,N1,N2,M2,M2,N3,M2,N3,N1,M2,N0,N1,r4B2A1,F2)
       allocate(U33(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K6*K0*K1
       call DGEMM(I1,I2,I3,D1,F2,U33)
       deallocate(D1)
       deallocate(F2)
C
       call
     & sum2341(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U33,-1.000)
       deallocate(U33)
C
       allocate(D1(N0+1:N1,M2+1:N3,N1+1:M2,M2+1:N3))
       call reorder4123(N2,N3,N1,N3,N2,N3,N0,N1,
     & N0,N1,M2,N3,N1,M2,M2,N3,HBHPPP,D1)
       allocate(F2(N0+1:N1,M2+1:N3,N1+1:M2,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder513246(N2,N3,N1,N3,N1,M2,N1,M2,N0,N1,N0,N1,
     & N0,N1,M2,N3,N1,M2,M2,N3,N1,M2,N0,N1,r4B2A,F2)
       allocate(U34(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K9*K6*K1
       call DGEMM(I1,I2,I3,D1,F2,U34)
       deallocate(D1)
       deallocate(F2)
C
       call
     & sum2341(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U34, 1.000)
       deallocate(U34)
C
       allocate(D1(N0+1:N1,N2+1:M2,N1+1:M2,M2+1:N3))
       call reorder4123(N2,N3,N1,N3,N2,N3,N0,N1,
     & N0,N1,N2,M2,N1,M2,M2,N3,HBHPPP,D1)
       allocate(F2(N0+1:N1,N2+1:M2,N1+1:M2,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder513246(N2,N3,N1,N3,N1,M2,N1,M2,N0,N1,N0,N1,
     & N0,N1,N2,M2,N1,M2,M2,N3,N1,M2,N0,N1,r4B2A,F2)
       allocate(U35(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K9*K0*K1
       call DGEMM(I1,I2,I3,D1,F2,U35)
       deallocate(D1)
       deallocate(F2)
C
       call
     & sum2341(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U35, 1.000)
       deallocate(U35)
C
       allocate(B1(N0+1:N2,N2+1:M2))
       call reorder21(N2,N3,N0,N2,
     & N0,N2,N2,M2,HBHP,B1)
       allocate(F2(N0+1:N2,N2+1:M2,M2+1:N3,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder521346(N2,N3,N2,M2,N1,N3,N1,M2,N0,N2,N0,N1,
     & N0,N2,N2,M2,M2,N3,M2,N3,N1,M2,N0,N1,r4C2B,F2)
       allocate(U36(M2+1:N3,M2+1:N3,N1+1:M2,N0+1:N1))
       I2=K1*K9*K6*K6
       I3=K0*K2
       call DGEMM2(I2,I3,B1,F2,U36)
       deallocate(B1)
       deallocate(F2)
C
       V3B1A=V3B1A-U36
       deallocate(U36)
C
       allocate(D1(N0+1:N2,N0+1:N1,N2+1:M2,N0+1:N1))
       call reorder3412(N2,N3,N0,N1,N0,N2,N0,N1,
     & N0,N2,N0,N1,N2,M2,N0,N1,HBHHHP,D1)
       allocate(F2(N0+1:N2,N0+1:N1,N2+1:M2,M2+1:N3,M2+1:N3,N1+1:M2))
       call reorder562134(N2,N3,N2,M2,N1,N3,N1,M2,N0,N2,N0,N1,
     & N0,N2,N0,N1,N2,M2,M2,N3,M2,N3,N1,M2,r4C2B,F2)
       allocate(U37(M2+1:N3,M2+1:N3,N1+1:M2,N0+1:N1))
       I1=K1
       I2=K9*K6*K6
       I3=K0*K1*K2
       call DGEMM(I1,I2,I3,D1,F2,U37)
       deallocate(D1)
       deallocate(F2)
C
       V3B1A=V3B1A+U37
       deallocate(U37)
C
       allocate(D1(N0+1:N2,N2+1:M2,M2+1:N3,M2+1:N3))
       call reorder3124(N2,N3,N1,N3,N0,N2,N1,N3,
     & N0,N2,N2,M2,M2,N3,M2,N3,HBPHPP,D1)
       allocate(F2(N0+1:N2,N2+1:M2,M2+1:N3,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder523146(N2,N3,N2,M2,N1,N3,N1,M2,N0,N2,N0,N1,
     & N0,N2,N2,M2,M2,N3,M2,N3,N1,M2,N0,N1,r4C2B,F2)
       allocate(U38(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K6*K0*K2
       call DGEMM(I1,I2,I3,D1,F2,U38)
       deallocate(D1)
       deallocate(F2)
C
       call
     & sum1342(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U38,-1.000)
       deallocate(U38)
C
       allocate(D1(N0+1:N2,M2+1:N3,N1+1:M2,M2+1:N3))
       call reorder3124(N2,N3,N1,N3,N0,N2,N1,N3,
     & N0,N2,M2,N3,N1,M2,M2,N3,HBPHPP,D1)
       allocate(F2(N0+1:N2,M2+1:N3,N1+1:M2,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder513246(M2,N3,M2,N3,N1,M2,N1,M2,N0,N2,N0,N1,
     & N0,N2,M2,N3,N1,M2,M2,N3,N1,M2,N0,N1,r4C2B1,F2)
       allocate(U39(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K9*K6*K2
       call DGEMM(I1,I2,I3,D1,F2,U39)
       deallocate(D1)
       deallocate(F2)
C
       call
     & sum1342(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U39, 1.000)
       deallocate(U39)
C
       allocate(D1(N0+1:N2,N2+1:M2,N1+1:M2,M2+1:N3))
       call reorder3124(N2,N3,N1,N3,N0,N2,N1,N3,
     & N0,N2,N2,M2,N1,M2,M2,N3,HBPHPP,D1)
       allocate(F2(N0+1:N2,N2+1:M2,N1+1:M2,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder523146(N2,N3,N2,M2,N1,N3,N1,M2,N0,N2,N0,N1,
     & N0,N2,N2,M2,N1,M2,M2,N3,N1,M2,N0,N1,r4C2B,F2)
       allocate(U40(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K9*K0*K2
       call DGEMM(I1,I2,I3,D1,F2,U40)
       deallocate(D1)
       deallocate(F2)
C
       call
     & sum1342(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U40,-1.000)
       deallocate(U40)
C
       allocate(D1(N0+1:N2,N2+1:M2,N1+1:M2,N1+1:M2))
       call reorder3124(N2,N3,N1,N3,N0,N2,N1,N3,
     & N0,N2,N2,M2,N1,M2,N1,M2,HBPHPP,D1)
       allocate(F2(N0+1:N2,N2+1:M2,N1+1:M2,M2+1:N3,M2+1:N3,N0+1:N1))
       call reorder524136(N2,N3,N2,M2,N1,N3,N1,M2,N0,N2,N0,N1,
     & N0,N2,N2,M2,N1,M2,M2,N3,M2,N3,N0,N1,r4C2B,F2)
       allocate(U41(M2+1:N3,M2+1:N3,N0+1:N1,N1+1:M2))
       I1=K9
       I2=K1*K6*K6
       I3=K9*K0*K2
       call DGEMM(I1,I2,I3,D1,F2,U41)
       deallocate(D1)
       deallocate(F2)
C
       call
     & sum1243(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U41,-1.000)
       deallocate(U41)
C
       allocate(D1(N0+1:N2,M2+1:N3,N2+1:M2,M2+1:N3))
       call reorder4123(N2,N3,N2,N3,N2,N3,N0,N2,
     & N0,N2,M2,N3,N2,M2,M2,N3,HCHPPP,D1)
       allocate(F2(N0+1:N2,M2+1:N3,N2+1:M2,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder512346(N2,N3,N2,M2,N1,N3,N1,M2,N0,N2,N0,N1,
     & N0,N2,M2,N3,N2,M2,M2,N3,N1,M2,N0,N1,r4C2B,F2)
       allocate(U42(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K0*K6*K2
       call DGEMM(I1,I2,I3,D1,F2,U42)
       deallocate(D1)
       deallocate(F2)
C
       call
     & sum2341(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U42,-1.000)
       deallocate(U42)
C
       allocate(D1(N0+1:N2,N2+1:M2,N2+1:M2,M2+1:N3))
       call reorder4123(N2,N3,N2,N3,N2,N3,N0,N2,
     & N0,N2,N2,M2,N2,M2,M2,N3,HCHPPP,D1)
       allocate(F2(N0+1:N2,N2+1:M2,N2+1:M2,M2+1:N3,N1+1:M2,N0+1:N1))
       call reorder512346(N2,N3,N2,M2,N1,N3,N1,M2,N0,N2,N0,N1,
     & N0,N2,N2,M2,N2,M2,M2,N3,N1,M2,N0,N1,r4C2B,F2)
       allocate(U43(M2+1:N3,N1+1:M2,N0+1:N1,M2+1:N3))
       I1=K6
       I2=K1*K9*K6
       I3=K0*K0*K2
       call DGEMM(I1,I2,I3,D1,F2,U43)
       deallocate(D1)
       deallocate(F2)
C
       call
     & sum2341(M2,N3,M2,N3,N1,M2,N0,N1,V3B1A,U43,-0.500)
       deallocate(U43)
C
       call VtoHR1234(M2,N3,M2,N3,N1,M2,N0,N1,
     & N2,N3,N1,N3,N1,N3,N0,N1,V3B1A,HR3B1A)
       deallocate(V3B1A)
C
       end
